import { TuiButtonComponent, TuiButtonModule, TuiRootModule, TuiScrollbarModule,TuiTextfieldControllerModule } from "@taiga-ui/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TuiRingChartModule} from '@taiga-ui/addon-charts';
import {TuiCarouselModule, TuiIslandModule, TuiPaginationModule,TuiInputModule, TuiTextAreaModule} from '@taiga-ui/kit';
import { ReactiveFormsModule } from "@angular/forms";
import {AnimateInDirective} from "./appear.directive"
import {ObserverService} from "./observer.service"
import { BreakpointObserverService } from "./breakpoint-observer.service";

@NgModule({
  declarations: [
    AppComponent,
    AnimateInDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
      BrowserAnimationsModule,
      TuiRootModule,
      TuiButtonModule,
      TuiScrollbarModule,
      TuiRingChartModule,
      TuiCarouselModule,
      TuiIslandModule,
      TuiPaginationModule,
      ReactiveFormsModule,
      TuiInputModule,
      TuiTextAreaModule,
      TuiTextfieldControllerModule
],
  providers: [ObserverService,BreakpointObserverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
