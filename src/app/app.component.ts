import { ViewportScroller } from '@angular/common';
import { Component, OnInit, ViewEncapsulation,HostListener } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BreakpointObserverService } from "./breakpoint-observer.service";
import {Observable} from 'rxjs';
export interface Title{
  id:number,
  name:string,
  route:string
}
export interface Body{
  id:number,
  name:string,
  img:string
}

export interface KeyValue{
  key:string,
  value:string
}

export interface IconValue{
  icon:string,
  value:string
}
export interface report{
  id:number,
  value: number,
  title: string
}
export interface Services{
  id:number,
  icon:string,
  title:string,
  body:string
}
export interface img{
  id: number,
  img:string
}
export interface square{
   img:string,
   text:string,
   name:string,
   position:string
}
export interface list{
  img:string,
  date:string,
  title:string,
  text:string
}
export interface infor{
  icon:string,
  title:string,
  sub:string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class AppComponent implements OnInit{
  public size$: Observable<string>;
  isMenu:boolean = false;
  public innerWidth: any;

  constructor(private scroller: ViewportScroller,private _breakpointObserverService: BreakpointObserverService) {
    this.size$ = this._breakpointObserverService.size$;

  }

  changeMenu(){
    this.isMenu = !this.isMenu;
  }

@HostListener('window:resize', ['$event'])
onResize(event:any) {
  this.innerWidth = window.innerWidth;
}

  navigate(route:string){
    const element = document.querySelector(`#${route}`)
    if (element) element.scrollIntoView({ behavior: 'smooth', block: 'start' })

  }

  title = 'myProfile';
  select: Title|null =null;
  readonly value = [100];
  index = 0;
  readonly testForm = new FormGroup({
    name : new FormControl(),
    email: new FormControl(),
    subject: new FormControl(),
    message:new FormControl()
});
  arr: Title[]= [
    {
      id: 1,
      name: "HOME",
      route:'home'
    },
    {
      id:2,
      name:"ABOUT",
      route:'about'

    },
    {
      id:3,
      name:"SKILLS",
      route:'skill'

    },
    {
      id:4,
      name:"SERVICE",
      route:'service'

    },
    {
      id:5,
      name:"PROJECT",
      route:'project'

    },
    {
      id:6,
      name:"BLOG",
      route:'blog'

    },
    {
      id:7,
      name:"CONTACT",
      route:'contact'

    }
  ];
  
  listIcon: Body[]=[
    {
      id:750,
      name:"PROJECT COMPLETE",
      img:"bi-briefcase"

    },
    {
      id:568,
      name:"HAPPY CLIENTS",
      img:"bi-emoji-laughing"

    },
    {
      id:478,
      name:"CUPS OF COFFEE",
      img:"bi-cup"

    },
    {
      id:780,
      name:"YEARS EXPERIENCED",
      img:"bi-graph-up-arrow"

    }
  ]

  keyValueList: KeyValue[]=[
    {
      key:"Name:",
      value:"Clyde Nowitzki"
    },
    {
      key:"Date of birth:",
      value:"January 01, 1990"
    },
    {
      key:"Address:",
      value:"San Francisco CA 97987 USA"
    },
    {
      key:"Zip code::",
      value:"1000"
    },
    {
      key:"Email:",
      value:"cydenowitzki@gmail.com"
    },
    {
      key:"Phone:",
      value:"+1-2234-5678-9-0"
    },
  ]

  iconValue: IconValue[]=[
    {
      icon:"headphones",
      value:"Music"
    },
    {
      icon:"card_travel",
      value:"Travel"
    },
    {
      icon:"movie",
      value:"Movie"
    },
    {
      icon:"sports_soccer",
      value:"Sports"
    }
  ]

  reportList: report[]=[
    {
      id:1,
      title:"CSS",
      value:95
    },
    {
      id:2,
      title:"HTML",
      value:98
    },
    {
      id:3,
      title:"jQuery",
      value:68
    },
    {
      id:4,
      title:"Photoshop",
      value:92
    },
    {
      id:5,
      title:"WordPress",
      value:83
    },
    {
      id:6,
      title:"SEO",
      value:95
    }
  ]

serviceList:Services[]=[
  {
    id:1,
    icon:"padding",
    title:"Web Design",
    body:"A small river named Duden flows by their place and supplies."
  },
  {
    id:2,
    icon:"radio",
    title:"Web Application",
    body:"A small river named Duden flows by their place and supplies."
  },
  {
    id:3,
    icon:"newspaper",
    title:"Web Development",
    body:"A small river named Duden flows by their place and supplies."
  },
  {
    id:4,
    icon:"subscriptions",
    title:"Banner Design",
    body:"A small river named Duden flows by their place and supplies."
  },
  {
    id:5,
    icon:"computer",
    title:"Branding",
    body:"A small river named Duden flows by their place and supplies."
  },
  {
    id:6,
    icon:"polyline",
    title:"Icon Design",
    body:"A small river named Duden flows by their place and supplies."
  },
  {
    id:7,
    icon:"polyline",
    title:"Graphic Design",
    body:"A small river named Duden flows by their place and supplies."
  },
  {
    id:8,
    icon:"pageview",
    title:"Seo",
    body:"A small river named Duden flows by their place and supplies."
  }
]

imgList: img[]=[
  {
    id:1,
    img:"https://preview.colorlib.com/theme/clyde/images/xwork-1.jpg.pagespeed.ic.m9NvzLIP04.webp"
  },
  {
    id:2,
    img:"https://preview.colorlib.com/theme/clyde/images/xwork-2.jpg.pagespeed.ic.T4blWGk2YN.webp"
  },
  {
    id:3,
    img:"https://preview.colorlib.com/theme/clyde/images/xwork-3.jpg.pagespeed.ic.0i57uSDQO7.webp"
  },
  {
    id:4,
    img:"https://preview.colorlib.com/theme/clyde/images/xwork-4.jpg.pagespeed.ic.KHHy2ZJtGx.webp"
  },
  {
    id:5,
    img:"https://preview.colorlib.com/theme/clyde/images/xwork-5.jpg.pagespeed.ic.qRp3UY8ZNs.webp"
  },
  {
    id:6,
    img:"https://preview.colorlib.com/theme/clyde/images/xwork-6.jpg.pagespeed.ic.Y6d53ULzYl.webp"
  },
  {
    id:7,
    img:"https://preview.colorlib.com/theme/clyde/images/xwork-6.jpg.pagespeed.ic.Y6d53ULzYl.webp"
  },
  {
    id:8,
    img:"https://preview.colorlib.com/theme/clyde/images/xwork-8.jpg.pagespeed.ic.QDSZSnfOS4.webp"
  }
]

listSquare: square[]=[
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_2.jpg.pagespeed.ic.Xrdu_nPZRp.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_1.jpg.pagespeed.ic.a2MnMHMs44.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_3.jpg.pagespeed.ic.Cln-jaM1jK.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_2.jpg.pagespeed.ic.Xrdu_nPZRp.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_1.jpg.pagespeed.ic.a2MnMHMs44.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_3.jpg.pagespeed.ic.Cln-jaM1jK.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_2.jpg.pagespeed.ic.Xrdu_nPZRp.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_1.jpg.pagespeed.ic.a2MnMHMs44.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_3.jpg.pagespeed.ic.Cln-jaM1jK.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_2.jpg.pagespeed.ic.Xrdu_nPZRp.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_1.jpg.pagespeed.ic.a2MnMHMs44.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_3.jpg.pagespeed.ic.Cln-jaM1jK.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_2.jpg.pagespeed.ic.Xrdu_nPZRp.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_1.jpg.pagespeed.ic.a2MnMHMs44.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_3.jpg.pagespeed.ic.Cln-jaM1jK.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_2.jpg.pagespeed.ic.Xrdu_nPZRp.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_1.jpg.pagespeed.ic.a2MnMHMs44.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/xperson_3.jpg.pagespeed.ic.Cln-jaM1jK.webp",
    text:"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.",
    name:"Roger Scott",
    position:"Marketing Manager"
  }
]
lists: list[]=[
  {
    img:"https://preview.colorlib.com/theme/clyde/images/ximage_1.jpg.pagespeed.ic.wkbgK5sCWT.webp",
    date:"JULY 03, 2020 ADMIN 3",
    title:"Why Lead Generation is Key for Business Growth",
    text:"A small river named Duden flows by their place and supplies it with the necessary regelialia."
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/ximage_2.jpg.pagespeed.ic.hPYaVjNW0H.webp",
    date:"JULY 03, 2020 ADMIN 3",
    title:"Why Lead Generation is Key for Business Growth",
    text:"A small river named Duden flows by their place and supplies it with the necessary regelialia."
  },
  {
    img:"https://preview.colorlib.com/theme/clyde/images/ximage_3.jpg.pagespeed.ic.XJ5IolSvSy.webp",
    date:"JULY 03, 2020 ADMIN 3",
    title:"Why Lead Generation is Key for Business Growth",
    text:"A small river named Duden flows by their place and supplies it with the necessary regelialia."
  }
]
inforList: infor[]=[
  {
    icon:"location_on",
    title:"Address:",
    sub:"198 West 21th Street, Suite 721 New York NY 10016"
  },
  {
    icon:"call",
    title:"Phone:",
    sub:"+ 1235 2355 98"
  },
    {
    icon:"near_me",
    title:"Email:",
    sub:"info@yoursite.com"
  },
  {
    icon:"public",
    title:"Website:",
    sub:"yoursite.com"
  }
]
  click(title:Title) {
    this.select= title;
    this.navigate(title.route);
  }
  ngOnInit() {
    this.select = this.arr[0];
    this.innerWidth = window.innerWidth;

  }
  
}
