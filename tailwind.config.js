module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {},
    container: {
      padding: {
        DEFAULT:'1rem',
        xl: '4rem',
        '2xl': '14rem',
      },
    },

  },
  plugins: [],
}